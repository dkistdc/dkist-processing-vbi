Changelog
*********

A complete record of the changes made for each version. Numbered links lead to the associated pull request.

To see only the scientifically important changes please see the :doc:`Scientific Changelog <scientific_changelog>`.

.. changelog::
    :towncrier:
    :towncrier-skip-if-empty:
    :changelog_file: ../CHANGELOG.rst
