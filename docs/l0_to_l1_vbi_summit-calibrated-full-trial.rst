l0_to_l1_vbi_summit-calibrated-full-trial
=========================================

This trial workflow is designed for pipeline testing internal to the DKIST Data Center (DC). It repackages the
already-calibrated data that is delivered to the data center.  The workflow stops short of publishing the results or
activating downstream DC services. The pipeline products are transferred to an internal location where they can be
examined by DC personnel or DKIST scientists.

For more detail on each workflow task, you can click on the task in the diagram.

.. workflow_diagram:: dkist_processing_vbi.workflows.trial_workflows.full_trial_summit_pipeline
