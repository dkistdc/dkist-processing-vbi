l0_to_l1_vbi_summit-calibrated
==============================

The VBI can operate in various modes, one of which is that raw data taken at the summit can be calibrated there before being delivered to the Data Center.
In this mode, the Data Center repackages the data for delivery to the scientific community and the following workflow is used.
In this workflow, the Data Center makes no changes to the contents of the FITS arrays.

For more detail on each workflow task, you can click on the task in the diagram.

.. workflow_diagram:: dkist_processing_vbi.workflows.summit_data_processing.summit_processed_data
